import logo from './logo.svg';
import './App.css';
import { TodoList } from "./Components/TodoList"

function App() {
  return (

    <div>
      <h1>Todo List</h1>
      <TodoList></TodoList>
    </div>
    
  );

}

export default App;
