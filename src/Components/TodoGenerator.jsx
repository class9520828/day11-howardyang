import { useState } from "react"

export function TodoGenerator(props) {

    const [text, setText] = useState('');

    const changeText = (inputText) => {
        setText(inputText)
    }
    function addInputText() {
        props.addText(text)
    }
    return (
        <div>
            <input type="text" value={text} onInput={(inputText) => changeText(inputText.target.value)}></input>
            <button onClick={addInputText}>Add</button>
        </div>
    )
}