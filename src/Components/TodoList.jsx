import { TodoGroup } from "./TodoGroup"
import { TodoGenerator } from "./TodoGenerator"
import { useState } from "react"

export function TodoList() {
  const [textList, setTextList] = useState([]);

  const addText = (inputText) => {
    console.log("dsad");
    setTextList(textList.concat(inputText))
  }

  return <>
    <TodoGroup textList={textList}></TodoGroup>
    <TodoGenerator addText={addText}></TodoGenerator>
  </>

}