import { TodoItem } from "./TodoItem"

export function TodoGroup(props) {

    return <>
        {props.textList.map((text, index) => (
            <TodoItem key={index} text={text}></TodoItem>
        ))}
    </>;
}