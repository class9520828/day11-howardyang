O(Objective):
+ In the morning, our group conducted a Code Review and resolved my issue with the application of the map method in the Stream API.
	
+ Our group conducted an activity to draw a conceptual map of Spring Boot. Through this group collaboration, I became clearer about the various components, methods, and concepts of Spring Boot, and received a large amount of goods.

+ I have learned HTML and CSS related knowledge, and am familiar with various operations of CSS selectors and property settings of CSS decorators through web games.
	
+ In the afternoon, I studied and practiced creating the first React project, and learned the relevant syntax of JSX.

R(Reflective):
+ Fruitful

I(Interpretive):
+ Because I haven't had any experience before, it's difficult to achieve the practice goals during all afternoon exercises.
+ I am not familiar with the concept of state in JSX and cannot understand the meaning of statements well.

D(Decision):
Continue practicing and consolidating the knowledge learned today.
